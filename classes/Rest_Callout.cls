public class Rest_Callout {
    
    public Account myAccount {get;set;}
    public String loginUri {get;set;}
    public String consumerKey {get;set;}
    public String consumerSecret {get;set;}
    public String userName {get;set;}
    public String password {get;set;}
    public String recordId {get;set;}
    
    public String endPoint {get;set;}
    public String accessToken {get;set;}
    
    public Rest_callout() {
        loginUri = 'https://login.salesforce.com';
        consumerKey = System.Label.SmartLabours_ConsumerKey;
        consumerSecret = System.Label.SmartLabours_ConsumerSecret;
        userName = System.Label.SmartLabours_UserName;
        password = System.Label.SmartLabours_Password;
        
        myAccount = new Account();
    }
    
    public void makeCalloutPost() {
        if(endPoint == null || accessToken == null) {
            makeOAuthCallout();
        }
        
        Map<String, String> jsonMap= new Map<String,String>();
        jsonMap.put('name', myAccount.Name);
        jsonMap.put('phone', myAccount.Phone);
        jsonMap.put('website', myAccount.Website);
        String jsonBody = JSON.serialize(jsonMap);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint+'/services/apexrest/Account');        
        req.setHeader('Authorization', 'OAuth '+ accessToken);                
        req.setMethod('POST');
        req.setBody(jsonBody);
        req.setHeader('Content-Type', 'application/json'); 
        Http http = new Http();
        HTTPResponse res = http.send(req);
        myAccount.Id = (String)JSON.deserializeUntyped(res.getBody());
        
        recordId = myAccount.Id;
        myAccount.Name = '';
        myAccount.Phone = '';
        myAccount.Website = '';
    }
    
    public void makeCalloutGet() {
        if(endPoint == null || accessToken == null) {
            makeOAuthCallout();
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint+'/services/apexrest/Account/'+myAccount.Id);        
        req.setHeader('Authorization', 'OAuth '+ accessToken);                
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        Map<String, Object> response = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        myAccount.Name = (String)response.get('Name');
        myAccount.Phone = (String)response.get('Phone');
        myAccount.Website = (String)response.get('Website');
    }
    
    public void makeCalloutDelete() {
        if(endPoint == null || accessToken == null) {
            makeOAuthCallout();
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint+'/services/apexrest/Account/'+myAccount.Id);        
        req.setHeader('Authorization', 'OAuth '+ accessToken);                
        req.setMethod('DELETE');
        Http http = new Http();
        http.send(req);
        
        myAccount.Id = null;
        recordId = '';
        myAccount.Name = '';
        myAccount.Phone = '';
        myAccount.Website = '';
    }
    
    private void makeOAuthCallout() {
        Map<String, Object> tokenResponse = oauthLogin(loginUri, consumerKey, consumerSecret, userName, password);
        endPoint = (String)tokenResponse.get('instance_url');
        accessToken = (String)tokenResponse.get('access_token');
    }
    
    private Map<String, Object> oauthLogin(String loginUri, String clientKey, String clientSecret, String username, String password) {
        HttpRequest req = new HttpRequest(); 
        req.setMethod('POST');
        req.setEndpoint(loginUri+'/services/oauth2/token');
        req.setBody('grant_type=password' +
            '&client_id=' + clientKey +
            '&client_secret=' + clientSecret +
            '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') +
            '&password=' + EncodingUtil.urlEncode(password, 'UTF-8'));
           
        Http http = new Http();        
        HTTPResponse res = http.send(req);

        return (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
    }
}