global class WS_ConsumeRESTPoint {

    public static void getContent(String temp) {
        Http h = new Http();
            
            // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://vivekdeepakironman-developer-edition.ap1.force.com/services/apexrest/GetCustomerInfo?CustomerName='+temp);
            req.setMethod('GET');
            req.setTimeout(120000); 
            
            
            HttpResponse res;
            
            if(!Test.isRunningTest()) {  //Escape the Callout from Test class
                 res = h.send(req);
                 system.debug(res.getBody());
            }
    }
    
}