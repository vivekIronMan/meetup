@RestResource(urlMapping='/PostAccountData')
global class WB_PostAccountInfo {
    global class AccountInfo {
        public Account account {get;set;}        
    }
    
    
	@HttpPost
    global static void sendSmsToCustomer() {
         RestRequest req = RestContext.request;
		 RestResponse res = RestContext.response;
         res.addHeader('Access-Control-Allow-Origin', '*');
		
		 
        try{            
            // Decerialize the post data which is coming from the website
            
            AccountInfo accountRecord = (AccountInfo)JSON.deserialize(req.requestBody.toString(),AccountInfo.class);            
            insert accountRecord.account;
			
        }catch(Exception e){
            system.debug('Error' + e.getMessage());                       
        } 	
        
    }
}