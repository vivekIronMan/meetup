@RestResource(urlMapping='/GetCustomerInfo/*')
global class WS_ShowAccountDetail {
    
    global class AccountRelatedInfo {
        public List<Account> accounts {get;set;}
        public List<Contact> contacts {get;set;}
        
        public AccountRelatedInfo(){
            contacts = new List<Contact>();
            accounts = new List<Account>();
        }
        
    }
    
	@HttpGet
    global static String showdetails() {
         RestRequest req = RestContext.request;
		 RestResponse res = RestContext.response;
         
		 String customerName = req.params.get('CustomerName');
		 
        try{
            List<List<SObject>> searchList = new List<List<SObject>>();
            String query = 'FIND \''+customerName+'\' RETURNING Account (Id,Name),Contact(Id,Name)';
            searchList = search.query(query); 
            
            AccountRelatedInfo accInfo = new AccountRelatedInfo();
            accInfo.accounts = (List<Account>)searchList[0];
            accInfo.contacts = (List<Contact>)searchList[1];
            
            
            return JSON.serialize(accInfo);
        }catch(Exception e){
            system.debug('Error' + e.getMessage());
            AccountRelatedInfo accInfo = new AccountRelatedInfo();            
            return JSON.serialize(accInfo);
            
        } 	
        
    }    
}